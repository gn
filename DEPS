vars = {
  'chromium_git': 'https://chromium.googlesource.com',
}

deps = {
  'gn/base':
    Var('chromium_git') + '/chromium/src/base.git@6ec23b2',
  'gn/build/config':
    Var('chromium_git') + '/chromium/src/build/config.git@7bbfc34',
  'gn/build/toolchain':
    Var('chromium_git') + '/chromium/src/build/toolchain.git@00dac9c',
  'gn/tools/gn':
    Var('chromium_git') + '/chromium/src/tools/gn.git@2db498a',
  'gn/testing/gtest':
    Var('chromium_git') + '/external/googletest.git@be1868139',
  'gn/testing/gmock':
   Var('chromium_git') + '/external/googlemock.git' + '@' + '29763965ab52f24565299976b936d1265cb6a271', # from svn revision 501
  'gn/third_party/icu':
   Var('chromium_git') + '/chromium/deps/icu.git' + '@' + 'eda9e75b1fa17f57ffa369ee3543a2301b68d0a9',
  'gn/tools/gyp':
    Var('chromium_git') + '/external/gyp.git' + '@' + 'd174d75bf69c682cb62af9187879e01513b35e52',

}

hooks = [
  # Pull GN binaries. This needs to be before running GYP below.
  {
    'name': 'gn_win',
    'pattern': '.',
    'action': [ 'download_from_google_storage',
                '--no_resume',
                '--platform=win32',
                '--no_auth',
                '--bucket', 'chromium-gn',
                '-s', 'gn/buildtools/win/gn.exe.sha1',
    ],
  },
  {
    'name': 'gn_mac',
    'pattern': '.',
    'action': [ 'download_from_google_storage',
                '--no_resume',
                '--platform=darwin',
                '--no_auth',
                '--bucket', 'chromium-gn',
                '-s', 'gn/buildtools/mac/gn.sha1',
    ],
  },
  {
    'name': 'gn_linux32',
    'pattern': '.',
    'action': [ 'download_from_google_storage',
                '--no_resume',
                '--platform=linux*',
                '--no_auth',
                '--bucket', 'chromium-gn',
                '-s', 'gn/buildtools/linux32/gn.sha1',
    ],
  },
  {
    'name': 'gn_linux64',
    'pattern': '.',
    'action': [ 'download_from_google_storage',
                '--no_resume',
                '--platform=linux*',
                '--no_auth',
                '--bucket', 'chromium-gn',
                '-s', 'gn/buildtools/linux64/gn.sha1',
    ],
  },
  {
    # Pull clang if needed or requested via GYP_DEFINES.
    # Note: On Win, this should run after win_toolchain, as it may use it.
    'name': 'clang',
    'pattern': '.',
    'action': ['python', 'gn/tools/clang/scripts/update.py', '--if-needed'],
  },
  {
    # Update LASTCHANGE. This is also run by export_tarball.py in
    # src/tools/export_tarball - please keep them in sync.
    'name': 'lastchange',
    'pattern': '.',
    'action': ['python', 'gn/build/util/lastchange.py',
               '-o', 'gn/build/util/LASTCHANGE'],
  },
]
